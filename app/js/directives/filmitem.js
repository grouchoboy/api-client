const directivesPath = './partials/directives';

export default function filmItem() {
  return {
    restrict: 'E',
    templateUrl: `${directivesPath}/film-item.html`,
  };
}
