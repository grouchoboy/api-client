const directivesPath = './partials/directives/';

export default function navigation(Auth, Session) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: `${directivesPath}navigation.html`,
    link(scope) {
      scope.$watch(() => {
        scope.session = Session.sessionData();
      });

      scope.logout = () => {
        Auth.logout();
        scope.session = Session.sessionData();
        window.location = '#/';
      };
    },
  };
}

navigation.$inject = ['Auth', 'Session'];
