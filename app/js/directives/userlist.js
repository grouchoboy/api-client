const directivesPath = './partials/directives';

export default function userList() {
  return {
    restrict: 'E',
    templateUrl: `${directivesPath}/user-list.html`,
  };
}
