import angular from 'angular';
import FooterComp from './components/footerComp';

const movieComponents = angular.module('movieComponents', []);

movieComponents.component('footerComp', FooterComp);

export default movieComponents;
