import angular from 'angular';
import { UsersCtrl, UsersDetailCtrl, UsersSettingsCtrl } from './controllers/usersctrl';
import { FilmsCtrl, FilmsDetailCtrl } from './controllers/filmsctrl';
import LoginCtrl from './controllers/loginctrl';
import HomeCtrl from './controllers/homeCtrl';

const movieControllers = angular.module('movieControllers', []);

movieControllers.controller('UsersCtrl', UsersCtrl)
    .controller('UsersDetailCtrl', UsersDetailCtrl)
    .controller('UsersSettingsCtrl', UsersSettingsCtrl)
    .controller('FilmsCtrl', FilmsCtrl)
    .controller('FilmsDetailCtrl', FilmsDetailCtrl)
    .controller('LoginCtrl', LoginCtrl)
    .controller('HomeCtrl', HomeCtrl);

export default movieControllers;
