import conf from '../../conf';
const endpoint = conf.devel.endpoint;

export default function Auth() {
  let logged = false;

  return {
    login(user, success) {
      const encodedCredentials = window.btoa(`${user.username}:${user.password}`);

      const req = new XMLHttpRequest();
      req.open('GET', `http://${endpoint}/users/login`, true);
      req.setRequestHeader('Authorization', `Basic ${encodedCredentials}`);

      req.addEventListener('load', () => {
        if (req.status === 200) {
          localStorage.setItem('logged', true);
          localStorage.setItem('username', user.username);
          localStorage.setItem('password', user.password);
          logged = true;
          success();
        }
      });

      req.send();
    },

    logout() {
      localStorage.removeItem('logged');
      localStorage.removeItem('username');
      localStorage.removeItem('password');
      logged = false;
    },

    loggedIn() {
      return logged;
    },

    currentUser: {
      username: localStorage.getItem('username'),
    },
  };
}
