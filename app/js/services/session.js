export default function Session() {
  return {
    sessionData: function sessionData() {
      return {
        logged: localStorage.getItem('logged'),
        username: localStorage.getItem('username'),
        password: localStorage.getItem('password'),
      };
    },
  };
}

Session.$inject = [];
