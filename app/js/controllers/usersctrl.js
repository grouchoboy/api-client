import conf from '../../conf';
import $ from 'jquery';
const endpoint = conf.devel.endpoint;

// Users Controller

export function UsersCtrl($scope, $http, Session) {
  $scope.page = 0;

  $http.get(`http://${endpoint}/users`).success((data) => {
    $scope.users = data.data;
    $scope.session = Session.sessionData();
  });

  $(window).scroll(() => {
    if ($(window).scrollTop() + $(window).height() === $(document).height()) {
      const page = $scope.page + 1;
      $http.get(`http://${endpoint}/users/page/${page}`)
        .success((data) => {
          $scope.users = $scope.users.concat(data.data);
          $scope.session = Session.sessionData();
          $scope.page += 1;
        });
    }
  });
}

UsersCtrl.$inject = ['$scope', '$http', 'Session'];

// Users Detail Controller

export function UsersDetailCtrl($scope, $stateParams, $http, Auth) {
  const getUsersEndpoint = `http://${endpoint}/users/${$stateParams.username}`;
  $http.get(getUsersEndpoint).success((data) => {
    $scope.user = data;
    $scope.logged = Auth.loggedIn();

  });

  const getReviewsEndpoint = `http://${endpoint}/users/${$stateParams.username}/reviews`;
  $http.get(getReviewsEndpoint)
    .success((data) => {
      $scope.reviews = data;
    });
}

UsersDetailCtrl.$inject = ['$scope', '$stateParams', '$http', 'Auth'];

// Users Settings Controller


export function UsersSettingsCtrl($scope, $http, Auth, Session) {
  if (!Auth.loggedIn()) {
    window.location = '#/';
    return;
  }

  $scope.session = Session.sessionData();
  $http.get(`http://${endpoint}/users/${Session.sessionData().username}`).success((data) => {
    $scope.user = data;
    $scope.username = data.username;
    $scope.description = data.description;
    $scope.logged = Auth.loggedIn();
    // $scope.$digest();
  });

  const updateBtn = document.querySelector('#settingsBtn');
  updateBtn.addEventListener('click', () => {
    const user = Session.sessionData();
    const encodedCredentials = window.btoa(`${user.username}:${user.password}`);

    const newName = document.settings.username.value;
    const newDescription = document.settings.description.value;

    $http.put(`http://${endpoint}/users/${user.username}`,
      { username: newName,
        description: newDescription
      },
      {
       headers: {'Authorization': `Basic ${encodedCredentials}`}
    }).then(function(response) {
      console.log(response);
    });
  });
}

UsersSettingsCtrl.$inject = ['$scope', '$http', 'Auth', 'Session'];
