
function LoginCtrl($scope, $rootScope, Auth, Session) {
  if (Auth.loggedIn() === true) {
    window.location = `#/users/${Auth.currentUser.username}`;
  }

  const button = document.getElementById('login');
  button.addEventListener('click', () => {
    const username = document.login.username.value;
    const password = document.login.password.value;

    Auth.login({ username, password }, () => {
      $scope.session = Session.sessionData();
      window.location = `#/users/${username}`;
    });
  });
}

LoginCtrl.$inject = ['$scope', '$rootScope', 'Auth', 'Session'];

export default LoginCtrl;
