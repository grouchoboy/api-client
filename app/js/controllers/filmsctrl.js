import conf from '../../conf';
import $ from 'jquery';
const endpoint = conf.devel.endpoint;

// Films Controller

export function FilmsCtrl($scope, $http) {
  $scope.page = 0;

  $http.get(`http://${endpoint}/films`).success((data) => {
    $scope.films = data.data;
  });

  $(window).scroll(() => {
    if ($(window).scrollTop() + $(window).height() === $(document).height()) {
      const page = $scope.page + 1;
      $http.get(`http://${endpoint}/films/page/${page}`)
       .success((data) => {
         $scope.films = $scope.films.concat(data.data);
         $scope.page += 1;
       });
    }
  });
}

FilmsCtrl.$inject = ['$scope', '$http'];

// Films Detail Controller

export function FilmsDetailCtrl($scope, $stateParams, $http, $route, Session) {
  $http.get(`http://${endpoint}/films/${$stateParams.id}/reviews`)
    .success((data) => {
      $scope.reviews = data;
      $scope.session = Session.sessionData();
      postReview();
    });

  function postReview() {
    const sendReviewBtn = document.getElementById('sendreview');
    sendReviewBtn.addEventListener('click', () => {
      const filmId = $stateParams.id;
      const title = document.reviewform.title.value;
      const body = document.reviewform.body.value;
      const user = Session.sessionData();
      const credentials = window.btoa(`${user.username}:${user.password}`);

      $http.post(`http://${endpoint}/reviews`,
        {
          filmId,
          title,
          body,
        },{
          headers: {
            'Authorization': `Basic ${credentials}`,
            'Content-Type': 'application/json',
          }
        }).then(function(response) {
          if(response.status === 201) {
            alert("Review creada correctamente");
          }
        });
    });
  }
}

FilmsDetailCtrl.$inject = ['$scope', '$stateParams', '$http', '$route', 'Session'];
