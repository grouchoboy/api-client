function HomeCtrl($scope, Session) {
  $scope.session = Session.sessionData();
  // $scope.$watch(() => {
  //       $scope.session = Session.sessionData();
  //     });
  if ($scope.session.logged) {
    console.log($scope.session);
    console.log($scope.session.username);
    window.location = `#/users/${$scope.session.username}`;
  }
  window.location = `#/login`;

}

HomeCtrl.$inject = ['$scope', 'Session'];

export default HomeCtrl;