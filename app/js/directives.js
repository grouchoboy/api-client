import angular from 'angular';
import navigation from './directives/navigation';
import userList from './directives/userlist';
import filmItem from './directives/filmitem';

const movieDirectives = angular.module('movieDirectives', []);

movieDirectives.directive('navigation', navigation);
movieDirectives.directive('userList', userList);
movieDirectives.directive('filmItem', filmItem);

export default movieDirectives;
