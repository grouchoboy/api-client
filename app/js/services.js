import angular from 'angular';
import Auth from './services/auth';
import Session from './services/session';

const movieServices = angular.module('movieServices', []);

movieServices.factory('Auth', Auth)
    .factory('Session', Session);

export default movieServices;
