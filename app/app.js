import angular from 'angular';
import ngRoute from 'angular-route';
import uiRoute from 'angular-ui-router';

import movieControllers from './js/controllers';
import movieServices from './js/services';
import movieDirectives from './js/directives';
import movieComponents from './js/components';

const movieApp = angular.module('movieApp', [
  'ui.router',
  'ngRoute',
  'movieControllers',
  'movieServices',
  'movieDirectives',
  'movieComponents',
]);

movieApp.config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider.
      state('home', {
        url: '/home',
        templateUrl: 'partials/home.html',
        controller: 'HomeCtrl',
      }).
      state('login', {
        url: '/login',
        templateUrl: 'partials/login.html',
        controller: 'LoginCtrl',
      }).
      state('users', {
        url: '/users',
        templateUrl: 'partials/users.html',
        controller: 'UsersCtrl',
      }).
      state('usersDetail', {
        url: '/users/:username',
        templateUrl: 'partials/user.html',
        controller: 'UsersDetailCtrl',
      }).
      state('usersSettingsCtrl', {
        url: '/users/:username/settings',
        templateUrl: 'partials/user-settings.html',
        controller: 'UsersSettingsCtrl',
      }).
      state('films', {
        url: '/films',
        templateUrl: 'partials/films.html',
        controller: 'FilmsCtrl',
      }).
      state('filmsDetail', {
        url: '/films/:id',
        templateUrl: 'partials/film.html',
        controller: 'FilmsDetailCtrl',
      });
  }]);
