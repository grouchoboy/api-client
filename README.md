# Práctica 5: Aplicaciones web en el lado del cliente
La práctica se encuentra en un repositorio git en:
 https://gitlab.com/grouchoboy/api-client

 Está colgada en http://manupascual.me:8080

 He intentado implementar una especie de scroll infinito tanto para los usuarios
 como para las películas. Una vez llegas al final de la página, hace una petición
 al servidor para obtener la siguiente página.

## Requisitos adicionales:
**Estilo visual de la web:** para el estilo visual he usado bootstrap junto con
un tema para el mismo. He intentado que al menos visualmente sea responsive,
realmente para que sea responsive habría que tener en cuenta muchos otros
factores, pero al menos el aspecto se comporta decentemente al cambiar de
tamaño. A parte también he usado sass en vez de css.

**División del código:** para dividir el código en módulos he usado webpack.
Además también le he puesto el loader de babel para que transforme el código
de es6.

**Herramientas de construcción en el cliente:** para este apartado también
he usado webpack. Aun que realmente no es un sustituto de grunt/gulp, si que
es cierto que para muchas de las tareas sí que lo es, y hay
[opiniones de todo tipo](https://www.reddit.com/r/javascript/comments/40j8ca/webpack_replace_gulpgrunt_plugins_with_a_single).
Así que con webpack junto a los siguientes scripts de npm han sido suficientes.

    // lanza los scripts definidos en server y build, este es el que se
    // usuaria cuando se está desarrolando
    "start": "npm run build & npm run server",

    // se encarga de lanzar un servidor http en localhost para el desarrollo
    "server": "webpack-dev-server --hot",

    // lanza webpack minificando y convirtiendo el código es6 en un javascript
    // compatible con la mayoría de navegadores y además lanza el script
    // definido en sass
    "build": "webpack -p & npm run sass",

    // hace transformar el codigo scss en css y se queda escuchando
    // a la espera de nuevos cambios
    "sass": "sass --watch app/css/styles.scss:app/build/bundle.css"