var pkg = require('./package.json');
var webpack = require('webpack');

module.exports = {
  cache: true,

  context: __dirname + "/app",

  resolve : { extensions: ['', '.jsx', '.js'] },

  entry: __dirname + "/app/app.js",

  devtool: "source-map",

  progress: true,

  output: {
    path: './app/build',
    filename: 'bundle.js',
    publicPath: "/app/build/"
  },

  devServer: {
    host        : '0.0.0.0',
    port        : 8080,
    inline      : true
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    })
  ],

  module: {
    loaders: [
      {
        test: /(\.js|\.jsx)$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: { presets: ['es2015'] }
      }
    ]
  }
};
